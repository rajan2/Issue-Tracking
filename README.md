# Issue Tracking
A Project that track your issue.

### Setup Information
First Clone Git Repo.
```sh
$ git clone https://gitlab.com/rajan2/Issue-Tracking.git
```
Please install node & npm if you don't have, follow this link 
https://www.npmjs.com/get-npm

then install npm modules using this
```sh
$ npm install
```

please install `http-server` globally if you don't have, using this command
```sh
$ npm install http-server -g
 ```
 
 # To Run this project follow this instruction
 
 Run http-server
 ```sh
 $ http-server
 ```
you will see your project in browser default url http://localhost:8080