let issuesString = localStorage.getItem('issues');
let currentPage = 1;
let issues = JSON.parse(issuesString);
function changePage(page){
  let nextButton = document.getElementById('next');
  let previousButton = document.getElementById('previous');
  let stringOfIssues = '';
  for (let i = (page - 1) * 5; i < (page * 5) && i < issues.length; i += 1) {
    stringOfIssues += `
      <tr>
        <td>${issues[i].title}</td>
        <td>${issues[i].sha}</td>
        <td>${issues[i].assign}</td>
        <td>${issues[i].date}</td>
        <td><span class="label label-success">open</span></td>
        <td>
          <button type="button" class="btn btn-success" onclick="parent.location='issueCreate.html'">
            Edit
          </button>
          <button type="button" class="btn btn-warning">
            Close
          </button>
        </td>
      </tr>`;
  }
  document.getElementById('table-body').innerHTML = stringOfIssues;

  let totalPage = Math.ceil(issues.length / 5);

  if (page === 1) {
    previousButton.style.display = 'none';
  } else {
    previousButton.style.display = 'inline';
  }
  if (page === totalPage) {
    nextButton.style.display = 'none';
  } else {
    nextButton.style.display = 'inline';
  }
}
// eslint-disable-next-line no-undef
$('#previous').click(function() {
  currentPage -= 1;
  changePage(currentPage);
});
// eslint-disable-next-line no-undef
$('#next').click(function() {
  currentPage += 1;
  changePage(currentPage);
});
