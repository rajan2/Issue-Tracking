/* eslint-env jquery */
/* issue Create */
$('#newIssue').submit(function() {
  let title = document.getElementById('issueTitle').value;
  let assign = document.getElementById('assign').value;

  if (localStorage.getItem('issues') === null) {
    let issues = [];
    let issueObject = {
      title: title,
      status: 'open',
      sha: new Date().getTime(),
      date: new Date(),
      assign: assign,
    };
    issues.push(issueObject);
    localStorage.setItem('issues', JSON.stringify(issues));
  } else {
    let issues = localStorage.getItem('issues');
    let issueArray = JSON.parse(issues);
    let issueObject = {
      title: title,
      status: 'open',
      sha: new Date().getTime(),
      date: new Date(),
      assign: assign,
    };
    issueArray.push(issueObject);
    localStorage.setItem('issues', JSON.stringify(issueArray));
  }
  document.getElementById('issueTitle').value = '';
  window.location.href = 'index.html';
  alert('Successfully Issue Created...!!!')
  return false;
});
